# DELETE ME AFTER March 2023

This repository is no longer needed, as manifest strategy changed in blueprints-ci: https://gitlab.com/Linaro/blueprints/ci/-/commit/0238d35ad9b3e5bde9db8181005004ed70fe89e3

I don't want to remove it just now because there might be some builds which could be re-tried and still use branches in this repo.
Remove it in March.

# Repo Manifests

This repository existed only to host temporary manifest files so that Blueprints CI can modify before sending it to TuxSuite.
